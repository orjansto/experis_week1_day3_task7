﻿using System;

namespace Task_7_Fibonacci
{
    class Program
    {
        /// <summary>
        /// Function for finding the sum of all even fibonacci numbers less than than the 
        /// limit
        /// </summary>
        /// <param name="a">Should be 0</param>
        /// <param name="b">Should be 1</param>
        /// <param name="limit">Lim for function</param>
        /// <param name="sum">Should be 0</param>
        /// <returns></returns>
        public static int sumEvenFibonacciLessThan(int a, int b, int limit, int sum)
        {
            //Console.WriteLine(a); (Only used for visualization)
            if(a < limit)
            {              
                sum = sumEvenFibonacciLessThan(b, a + b, limit,sum);
                if ((a % 2) == 0)
                {
                    sum += a;
                }
            }
            return sum;
        }
        
        static void Main(string[] args)
        {
            bool success = false;
            int limit;

            Console.WriteLine("--Task7--");
            Console.WriteLine("This program will write sum of even fibonacci" +
                " numbers less than given limit");
            do
            {
                Console.WriteLine("Please enter a limit:");
                success = Int32.TryParse(Console.ReadLine().Trim(), out limit);
            } while (!success);
            

            int sum = sumEvenFibonacciLessThan(0,1,limit,0);
            Console.WriteLine($"The sum of all even fibonnaci numbers less than {limit} is: {sum}");
        }
    }
}
